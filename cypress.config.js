const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl: 'http://outsourcing.nat.tepkom.ru:5080',
    host: 'outsourcing.nat.tepkom.ru:5080',
    viewportHeight: 1080,
    viewportWidth: 1920,
    defaultCommandTimeout: 10000,
    env: {
      "kuratorUserName": "kuratorov_ru"
    }
  },
});
