/// <reference types="cypress" />

describe('Login tests', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it.only('Test login', () => {
    cy.get('#mat-input-0').type(Cypress.env('kuratorUserName'));
    cy.get('#mat-input-1').type(Cypress.env('kuratorPass'));
    cy.get('.mat-focus-indicator').click()
    cy.get('.page__main-title').should((elem) => expect(elem).to.contain('Полевые проекты'))
    cy.get(':nth-child(3) > .global-menu-tree__leaf-link').should('have.css', 'color', 'rgb(0, 120, 210)')
    cy.get('.plan-execution-chart__title').should((elem) => expect(elem).to.contain('Выполнение плана по объемам'));
  })
  
})
