/// <reference types="cypress" />

describe('Verify important page render', () => {
    beforeEach(() => {
      cy.authorize(Cypress.env('kuratorUserName'), Cypress.env('kuratorPass'));
    })
  
    it('Render Полевые проекты page', () => {
      cy.visit('/');
      cy.get('.page__main-title').should((elem) => expect(elem).to.contain('Полевые проекты'))
    })

  })